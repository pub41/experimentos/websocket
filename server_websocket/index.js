const express = require('express');
const app = require("express")();
const httpServer = require("http").createServer(app);
const options = {
  cors: {
    origin: "*",
    methods: ["GET", "POST"]
  }
}
const io = require("socket.io")(httpServer, options);

app.get('/', (req, res) => {
  res.json({msg: 'Hello Websocket!'}, 200)
});

io.on('connection', (socket) => {
      socket.on('default_channel', (payload) => {
        setTimeout(() => {
          io.emit(payload.channel, payload.message);
        }, payload.time)
      })

})

httpServer.listen(3000, () => {
  console.log('listening on *:3000');
});
